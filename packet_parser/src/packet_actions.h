//
// Created by Alexander Strole on 3/17/19.
//

#ifndef PACKET_ACTIONS_H
#define PACKET_ACTIONS_H

// @TODO Change these characters to values above 127

enum e_data_values {
    SEPERATOR = '\x03',
    START_MESSAGE = '\x02',
    END_MESSAGE = '\x05'
};

enum e_session_action {
    REMOVE_FROM_QUEUE = '\x19',
    ADD_TO_QUEUE = '\x1a',
    SESSION_START = '\x1b',
    END_SESSION = '\x04',
    IN_SESSION = '\x22',
    CYCLE_ACTION = '\x23',
    PING = '\x24',
    SESSION_IDENTIFIER = '\x25',
    CLIENT_OK_START = '\x26'
};

enum e_cycle_action {
    CYCLE_POS = '\x1e',
    CYCLE_DIR = '\x1f',
    CYCLE_DIED = '\x20',
    CYCLE_IDENTIFIER = '\x21'
};

#endif
