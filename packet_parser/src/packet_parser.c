//
// Created by kohlten on 4/13/19.
//
#define COMMAND_PARSER_INTERNAL
#include "packet_parser.h"
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>

s32 is_valid_packet(const u8 *packet, u64 length, u64 *end) {
    u32 i, j;

    for (i = 0; i < length; i++) {
        if (packet[i] == START_MESSAGE) {
            for (j = i; j < length; j++)
                if (packet[j] == END_MESSAGE)
                    break;
            if (packet[j] != END_MESSAGE)
                return -1;
            if (end)
                *end = j;
            return i;
        }
    }
    return -1;
}

s32 parse_identifier(t_packet *packet_unpacked, const u8 *packet, u64 packet_length) {
    u32 i, start;
    u16 identifier;

    if (packet_length < 5)
        return -1;
    start = 3;
    for (i = 3; i < packet_length && isdigit(packet[i]); i++);
    if (packet[i] != END_MESSAGE)
        return -1;
    sscanf((char *) &packet[start], "%hu", &identifier);
    if (packet_unpacked->action == SESSION_IDENTIFIER)
        packet_unpacked->session_identifier = identifier;
    else
        packet_unpacked->cycle_identifier = identifier;
    return 0;
}

s32 parse_client_ok(t_packet *packet_unpacked, const u8* packet, u64 packet_length) {
    u32 i, start;

    if (packet_length < 5)
        return -1;
    start = 3;
    for (i = 3; i < packet_length && isdigit(packet[i]); i++);
    if (packet[i] != END_MESSAGE)
        return -1;
    sscanf((char *) &packet[start], "%hu", &packet_unpacked->session_identifier);
    return 0;
}

t_packet *parse_packet(const u8 *packet, u64 packet_length, u64 end) {
    t_packet *packet_unpacked;

    packet_unpacked = malloc(sizeof(t_packet));
    if (!packet_unpacked)
        return NULL;
    packet_unpacked->action = packet[1];
    switch (packet_unpacked->action) {
        case CYCLE_ACTION:
            if (parse_cycle_action(packet_unpacked, packet, packet_length) < 0)
                return NULL;
            break;
        case REMOVE_FROM_QUEUE:
        case ADD_TO_QUEUE:
        case SESSION_START:
        case END_SESSION:
            break;
        case SESSION_IDENTIFIER:
        case CYCLE_IDENTIFIER:
            if (parse_identifier(packet_unpacked, packet, packet_length) < 0)
                return NULL;
            break;
        case CLIENT_OK_START:
            if (parse_client_ok(packet_unpacked, packet, packet_length) != 0)
                return NULL;
            break;
        default:
            return NULL;
    }
    packet_unpacked->bytes = end;
    return packet_unpacked;
}

t_packet *commander_parser_parse(const u8 *packet, u64 packet_length) {
    s32 offset;
    u64 end;

    offset = is_valid_packet(packet, packet_length, &end);
    if (offset < 0)
        return NULL;
    return parse_packet(packet + offset, packet_length - offset, end);
}