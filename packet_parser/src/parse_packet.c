//
// Created by kohlten on 4/16/19.
//

#define COMMAND_PARSER_INTERNAL
#include "packet_parser.h"
#include <ctype.h>
#include <stdio.h>

s32 parse_cycle_identity(t_packet *packet_unpacked, const u8 *packet, u64 packet_length) {
    u32 i, start;

    if (packet_length < 9)
        return -1;
    packet_unpacked->cycle_action = packet[3];
    start = 5;
    i = start;
    while (i < packet_length && isdigit(packet[i]))
        i++;
    if (packet[i] != SEPERATOR)
        return -1;
    sscanf((char *) &packet[start], "%hu", &packet_unpacked->session_identifier);
    i++;
    start = i;
    while (i < packet_length && isdigit(packet[i]))
        i++;
    if (packet[i] != SEPERATOR)
        return -1;
    sscanf((char *) &packet[start], "%hu", &packet_unpacked->cycle_identifier);
    i++;
    return i;
}

s32 parse_cycle_pos(t_packet *packet_unpacked, const u8 *packet, u64 packet_length, s32 start) {
    u32 i;

    // @TODO Add max length here
    if (packet_length < 17)
        return -1;
    i = start;
    while (i < packet_length && (isdigit(packet[i]) || packet[i] == '.' || packet[i] == '-'))
        i++;
    if (packet[i] != SEPERATOR)
        return -1;
    sscanf((char *) &packet[start], "%lf", &packet_unpacked->pos.x);
    i++;
    start = i;
    while (i < packet_length && (isdigit(packet[i]) || packet[i] == '.' || packet[i] == '-'))
        i++;
    if (packet[i] != END_MESSAGE)
        return -1;
    sscanf((char *) &packet[start], "%lf", &packet_unpacked->pos.y);
    return 0;
}

s32 parse_cycle_dir(t_packet *packet_unpacked, const u8 *packet, u64 packet_length, s32 start) {
    u32 i;

    i = start;
    while (i < packet_length && isdigit(packet[i]))
        i++;
    if (packet[i] != END_MESSAGE)
        return -1;
    sscanf((char *) &packet[start], "%hu", &packet_unpacked->dir);
    return 0;
}

s32 parse_cycle_action(t_packet *packet_unpacked, const u8 *packet, u64 packet_length) {
    s32 start;

    start = parse_cycle_identity(packet_unpacked, packet, packet_length);
    if (start < 0)
        return -1;
    switch (packet_unpacked->cycle_action) {
        case CYCLE_POS:
            return parse_cycle_pos(packet_unpacked, packet, packet_length, start);
        case CYCLE_DIR:
            return parse_cycle_dir(packet_unpacked, packet, packet_length, start);
        default:
            return -1;
    }
}