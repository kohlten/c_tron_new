//
// Created by kohlten on 4/13/19.
//

#ifndef SERVER_COMMAND_PARSER_H
#define SERVER_COMMAND_PARSER_H

#include "number.h"
#include "packet_actions.h"
#include "vector.h"

typedef struct s_packet t_packet;

struct s_packet {
    u8 action;
    u8 cycle_action;
    u16 session_identifier;
    u16 cycle_identifier;
    u16 dir;
    bool b;
    t_vector2f pos;
    u64 bytes;
};

t_packet *commander_parser_parse(const u8 *packet, u64 packet_length);
s32 is_valid_packet(const u8 *packet, u64 length, u64 *end);

#ifdef COMMAND_PARSER_INTERNAL
s32 parse_cycle_identity(t_packet *packet_unpacked, const u8 *packet, u64 packet_length);
s32 parse_cycle_pos(t_packet *packet_unpacked, const u8 *packet, u64 packet_length, s32 start);
s32 parse_cycle_dir(t_packet *packet_unpacked, const u8 *packet, u64 packet_length, s32 start);
s32 parse_cycle_action(t_packet *packet_unpacked, const u8 *packet, u64 packet_length);
#endif

#endif //SERVER_COMMAND_PARSER_H
