//
// Created by kohlten on 4/16/19.
//

#ifndef SERVER_COMMANDS_H
#define SERVER_COMMANDS_H

#include "server_loop.h"
#include "packet_parser.h"

s32 map_command(t_packet *packet, t_server *server, t_tsocket *client);

#endif //SERVER_COMMANDS_H
