//
// Created by kohlten on 4/17/19.
//

#ifndef SERVER_NETWORK_PACKETS_H
#define SERVER_NETWORK_PACKETS_H

#include "threaded_socket.h"
#include "packet_parser.h"

void send_session_identifer(t_tsocket *client, u16 identifier);
void send_cycle_identifier(t_tsocket *client, u16 identifier);
void send_cycle_position(t_tsocket *client, t_vector2f pos, u16 session_id, u16 cycle_id);
void send_server_start(t_tsocket *client);

#endif //SERVER_NETWORK_PACKETS_H
