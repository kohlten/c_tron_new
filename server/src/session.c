//
// Created by kohlten on 4/16/19.
//

#include "session.h"
#include "cycle.h"
#include "threaded_socket.h"
#include "network_packets.h"

#include <stdlib.h>
#include <limits.h>
#include <stdio.h>


static void init_session(t_session *session) {
    t_tsocket *client;
    s32 i;

    for (i = 0; i < vector_total(session->clients); i++) {
        client = vector_get(session->clients, i);
        send_session_identifer(client, session->identifier);
        send_cycle_identifier(client, i);
    }
}

t_session *new_session(vector *clients, u16 id) {
    t_session *session;
    t_cycle *cycle;
    u32 i;

    session = malloc(sizeof(t_session));
    if (!session)
        return NULL;
    session->clients = clients;
    if (vector_init(&session->cycles) != 0)
        return NULL;
    session->players = vector_total(clients);
    for (i = 0; i < session->players; i++) {
         cycle = new_cycle(i);
         if (!cycle)
             return NULL;
         vector_add(&session->cycles, cycle);
    }
    session->clients_oked = 0;
    session->started = false;
    session->identifier = id;
    init_session(session);
    return session;
}

s32 update_session(t_session *session) {
    t_cycle *cycles[MAX_PLAYERS];
    t_tsocket *socks[MAX_PLAYERS];
    u32 i, j;

    for (i = 0; i < session->players; i++) {
        cycles[i] = vector_get(&session->cycles, i);
        socks[i] = vector_get(session->clients, i);
        if (!cycles[i] || !socks[i])
            return -1;
    }
    if (session->clients_oked == session->players && session->started) {
        for (i = 0; i < session->players; i++)
            cycle_update(cycles[i]);
        for (i = 0; i < session->players; i++) {
            for (j = 0; j < session->players; j++) {
                printf("Sent cycle pos %f %f of cycle %d to client %s\n", cycles[j]->position.x, cycles[j]->position.y, j, get_tsocket_address_tmp(socks[i]));
                send_cycle_position(socks[i], cycles[j]->position, session->identifier, i);
            }
        }
    } else {
        if (session->clients_oked == session->players) {
            for (i = 0; i < session->players; i++)
                send_server_start(socks[i]);
            session->started = true;
        }
    }
    return 0;
}

bool session_finished(t_session *session) {
    t_cycle *cycle;
    s32 i;
    s32 cycles_active;

    cycles_active = 0;
    for (i = 0; i < vector_total(&session->cycles); i++) {
        cycle = vector_get(&session->cycles, i);
        if (!cycle->died)
            cycles_active++;
    }
    return cycles_active <= 1;
}

void free_session(t_session *session) {
    t_cycle *cycle;
    s32 i;

    for (i = 0; i < vector_total(&session->cycles); i++) {
        cycle = vector_get(&session->cycles, i);
        free_cycle(cycle);
    }
    vector_free(&session->cycles);
    vector_free(session->clients);
    free(session->clients);
    free(session);
}