//
// Created by kohlten on 4/17/19.
//

#include "network_packets.h"

#include <stdio.h>

void send_session_identifer(t_tsocket *client, u16 identifier) {
    u8 packet[10];

    sprintf((char *) packet, "%c%c%c%hu%c", START_MESSAGE, SESSION_IDENTIFIER, SEPERATOR, identifier, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}

void send_cycle_identifier(t_tsocket *client, u16 identifier) {
    u8 packet[10];

    sprintf((char *) packet, "%c%c%c%hu%c", START_MESSAGE, CYCLE_IDENTIFIER, SEPERATOR, identifier, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}

void send_cycle_position(t_tsocket *client, t_vector2f pos, u16 session_id, u16 cycle_id) {
    u8 packet[40];

    sprintf((char *) packet, "%c%c%c%c%c%hu%c%hu%c%f%c%f%c", START_MESSAGE, CYCLE_ACTION, SEPERATOR, CYCLE_POS,
            SEPERATOR, session_id, SEPERATOR, cycle_id, SEPERATOR, pos.x,
            SEPERATOR, pos.y, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}

void send_server_start(t_tsocket *client) {
    u8 packet[4];

    sprintf((char *) packet, "%c%c%c", START_MESSAGE, SESSION_START, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}