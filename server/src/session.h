//
// Created by kohlten on 4/16/19.
//

#ifndef SERVER_SESSION_H
#define SERVER_SESSION_H

#include "vector_array.h"
#include "number.h"

#include <stdbool.h>

#define MAX_PLAYERS 4
#define MIN_PLAYERS 1

typedef struct s_session t_session;

struct s_session {
    vector *clients;
    vector cycles;
    u32 players;
    u16 identifier;
    u16 clients_oked;
    bool started;
};

t_session *new_session(vector *clients, u16 id);
s32 update_session(t_session *session);
bool session_finished(t_session *session);
void free_session(t_session *session);

#endif //SERVER_SESSION_H
