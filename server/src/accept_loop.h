//
// Created by kohlten on 4/13/19.
//

#ifndef SERVER_ACCEPT_LOOP_H
#define SERVER_ACCEPT_LOOP_H

#include "server_loop.h"

s32 new_accept_loop(t_server *server);
void free_accept_loop(t_server *server);

#endif //SERVER_ACCEPT_LOOP_H
