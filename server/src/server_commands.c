//
// Created by kohlten on 4/16/19.
//

#include "server_commands.h"
#include "session.h"
#include "cycle.h"

#include <stdio.h>

s32 add_to_queue(t_server *server, t_tsocket *client) {
    if (!server || !client)
        return -1;
    if (in_queue(&server->queue, client)) {
        printf("Client %s is already in the queue!\n", get_tsocket_address_tmp(client));
        return -1;
    }
    if (push_queue(&server->queue, client) != 0)
        return -1;
    printf("Added client %s to queue\n", get_tsocket_address_tmp(client));
    return 0;
}

s32 remove_from_queue(t_server *server, t_tsocket *client) {
    if (!server || !client)
        return -1;
    if (length_queue(&server->queue) == 0)
        return -1;
    printf("Removed client %s to queue\n", get_tsocket_address_tmp(client));
    return remove_queue(&server->queue, client, false, NULL);
}

s32 client_ok_start(t_server *server, t_tsocket *client, t_packet *packet) {
    u8 *id_str[6];
    s32 i;
    t_session *session;
    bool found;

    if (!server || !client)
        return -1;
    sprintf((char *)id_str, "%hu", packet->session_identifier);
    if (get_node_hashtable(&server->session_ids, (char *)id_str, NULL) != HASH_SUCCESS)
        return -1;
    found = false;
    for (i = 0; i < vector_total(&server->sessions); i++) {
        session = vector_get(&server->sessions, i);
        if (!session)
            return -1;
        if (session->identifier == packet->session_identifier) {
            found = true;
            break;
        }
    }
    if (!found)
        return -1;
    session->clients_oked++;
    return 0;
}

s32 cycle_change_dir(t_server *server, t_tsocket *client, t_packet *packet) {
    u8 *id_str[6];
    s32 i;
    t_session *session;
    t_cycle *cycle;
    bool found;

    if (!server || !client)
        return -1;
    sprintf((char *)id_str, "%hu", packet->session_identifier);
    if (get_node_hashtable(&server->session_ids, (char *)id_str, NULL) != HASH_SUCCESS)
        return -1;
    found = false;
    for (i = 0; i < vector_total(&server->sessions); i++) {
        session = vector_get(&server->sessions, i);
        if (!session)
            return -1;
        if (session->identifier == packet->session_identifier) {
            found = true;
            break;
        }
    }
    if (!found)
        return -1;
    cycle = vector_get(&session->cycles, packet->cycle_identifier);
    if (!cycle)
        return -1;
    cycle->dir = packet->dir;
    return 0;
}

s32 map_cycle_command(t_packet *packet, t_server *server, t_tsocket *client) {
    switch (packet->cycle_action) {
        case CYCLE_DIR:
            return cycle_change_dir(server, client, packet);
    }
    return -1;
}

s32 map_command(t_packet *packet, t_server *server, t_tsocket *client) {
    switch (packet->action) {
        case ADD_TO_QUEUE:
            return add_to_queue(server, client);
        case REMOVE_FROM_QUEUE:
            return remove_from_queue(server, client);
        case CLIENT_OK_START:
            return client_ok_start(server, client, packet);
        case CYCLE_ACTION:
            return map_cycle_command(packet, server, client);
    }
    return -1;
}