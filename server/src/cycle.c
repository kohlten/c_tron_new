//
// Created by Alexander Strole on 3/17/19.
//

#include "cycle.h"

#include <stdlib.h>
#include <strings.h>
#include <stdio.h>

t_cycle *new_cycle(int loc) {
    t_cycle *cycle;

    cycle = malloc(sizeof(t_cycle));
    if (!cycle)
        return NULL;
    bzero(cycle, sizeof(t_cycle));
    //vector_init(&cycle->previous_positions);
    switch (loc) {
        case 3:
            cycle->dir = CYCLE_DOWN;
            cycle->position = new_vector2f(0, 0);
            break;
        case 2:
            cycle->dir = CYCLE_LEFT;
            cycle->position = new_vector2f(0, MAP_HEIGHT - 1);
            break;
        case 1:
            cycle->dir = CYCLE_UP;
            cycle->position = new_vector2f(MAP_WIDTH - 1, MAP_HEIGHT - 1);
            break;
        case 0:
            cycle->dir = CYCLE_RIGHT;
            cycle->position = new_vector2f(MAP_WIDTH - 1, 0);
            break;
        default:
            printf("Invalid cycle start location %d\n", loc);
    }
    cycle->died = false;
    return cycle;
}

void cycle_update(t_cycle *cycle) {
    if (!cycle->died) {
        switch (cycle->dir) {
            case CYCLE_DOWN:
                cycle->position.y += CYCLE_MOVESPEED;
                break;
            case CYCLE_UP:
                cycle->position.y -= CYCLE_MOVESPEED;
                break;
            case CYCLE_LEFT:
                cycle->position.x += CYCLE_MOVESPEED;
                break;
            case CYCLE_RIGHT:
                cycle->position.x -= CYCLE_MOVESPEED;
                break;
        }
    }
    //if (cycle->position.y < 0 || cycle->position.y > MAP_HEIGHT || cycle->position.x < 0 ||
    //    cycle->position.x > MAP_WIDTH)
    //    cycle->died = 1;
}

void free_cycle(t_cycle *cycle) {
    free(cycle);
}