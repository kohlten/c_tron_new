//
// Created by Alexander Strole on 3/17/19.
//

#ifndef TRON_CYCLE_H
#define TRON_CYCLE_H

#include "vector.h"
#include "number.h"
#include "vector_array.h"

#define CYCLE_MOVESPEED 0.1
#define MAP_HEIGHT 400
#define MAP_WIDTH 400

typedef enum e_cycle_dir {
    CYCLE_UP = 1,
    CYCLE_DOWN = 2,
    CYCLE_LEFT = 3,
    CYCLE_RIGHT = 4
}           t_cycle_dir;

typedef struct s_cycle {
    t_vector2f position;
    t_cycle_dir dir;
    bool died;
}               t_cycle;

t_cycle *new_cycle(int loc);
void cycle_update(t_cycle *cycle);
void free_cycle(t_cycle *cycle);

#endif //TRON_CYCLE_H
