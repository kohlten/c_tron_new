//
// Created by kohlten on 4/13/19.
//

#include "accept_loop.h"

#include <pthread.h>
#include <stdlib.h>

static void *accept_loop(void *ptr) {
    t_server *server;
    t_tsocket *client;
    t_tsocket_status status;

    server = (t_server *)ptr;
    client = malloc(sizeof(t_tsocket));
    if (!client) {
        server->status = -1;
        return NULL;
    }
    while (server->running) {
        status = init_tsocket_client_from_server(client, server->server);
        if (status != TSOCKET_OK) {
            server->status = -1;
            return NULL;
        }
        if (vector_add(&server->clients, client) != 0) {
            server->status = -1;
            return NULL;
        }
        client = malloc(sizeof(t_tsocket));
        if (!client) {
            server->status = -1;
            return NULL;
        }
    }
    return NULL;
}

s32 new_accept_loop(t_server *server) {
    if (pthread_create(&server->accept_loop, NULL, accept_loop, server) != 0)
        return -1;
    return 0;
}

void free_accept_loop(t_server *server) {
    pthread_join(server->accept_loop, NULL);
}