//
// Created by kohlten on 4/13/19.
//

#include "server_loop.h"

#include "accept_loop.h"
#include "packet_parser.h"
#include "threaded_socket_time.h"
#include "server_commands.h"
#include "session.h"

#include <stdio.h>
#include <stdlib.h>

s32 init_server(t_server *server, t_socket *server_sock) {
    if (!server || !server_sock)
        return -1;
    server->server = server_sock;
    if (vector_init(&server->clients) != 0)
        return -1;
    if (vector_init(&server->sessions) != 0)
        return -1;
    if (init_queue(&server->queue) != 0)
        return -1;
    server->running = true;
    server->status = 0;
    if (new_accept_loop(server) != 0)
        return -1;
    if (init_hashtable(&server->session_ids, 0) != HASH_SUCCESS)
        return -1;
    return 0;
}

s32 receive_and_parse_packet(t_server *server, t_tsocket *client) {
    u8 *packet;
    u8 *packet_copy;
    u64 packet_length, end;
    s64 packet_start;
    t_packet *packet_unpacked;
    t_tsocket_status status;

    packet = peek_data_tsocket(client, &packet_length, &status);
    if (!packet || status != TSOCKET_OK)
        return -1;
    if ((packet_start = is_valid_packet(packet, packet_length, &end)) >= 0) {
        packet = get_data_tsocket(client, &packet_length, &status);
        if (!packet || status != TSOCKET_OK)
            return -1;
        packet_copy = packet;
        do {
            packet_unpacked = commander_parser_parse(packet + packet_start, packet_length);
            packet_copy += end;
            if (packet_unpacked == NULL) {
                printf("Failed to parse command from client %s\n", get_tsocket_address_tmp(client));
                continue;
            }
            if (map_command(packet_unpacked, server, client) != 0)
                printf("Failed to map command %d from client %s\n", packet_unpacked->action,
                       get_tsocket_address_tmp(client));
        } while ((packet_start = is_valid_packet(packet_copy, packet_length, &end)) >= 0);
        free(packet);
    }
    return 0;
}

// @TODO Separate and put into separate file
s32 parse_packets(t_server *server) {
    s32 i;
    t_tsocket *client;
    t_tsocket_status status;

    for (i = 0; i < vector_total(&server->clients); i++) {
        client = vector_get(&server->clients, i);
        if (client) {
            if (threads_running_tsocket(client, &status) != true)
                return -1;
            if (data_available_tsocket(client))
                if (receive_and_parse_packet(server, client) != 0)
                    return -1;
        }
    }
    return 0;
}

s32 create_new_session(t_server *server) {
    t_tsocket *client;
    vector *session_clients;
    t_session *session;
    s32 i;
    u16 id;
    u8 session_id_str[6];

    session_clients = malloc(sizeof(vector));
    if (!session_clients)
        return -1;
    if (vector_init(session_clients) != 0)
        return -1;
    for (i = 0; i < MAX_PLAYERS && length_queue(&server->queue) > 0; i++) {
        if (pop_queue(&server->queue, (void **) &client) != 0)
            return -1;
        if (vector_add(session_clients, client) != 0)
            return -1;
    }
    do {
        id = lrand48() % UINT16_MAX;
        sprintf((char *) session_id_str, "%hu", id);
    } while (id == 0 || get_node_hashtable(&server->session_ids, (char *) session_id_str, NULL) == HASH_SUCCESS);
    session = new_session(session_clients, id);
    if (!session)
        return -1;
    if (vector_add(&server->sessions, session) != 0)
        return -1;
    if (add_node_hashtable(&server->session_ids, (char *) session_id_str, NULL) != HASH_SUCCESS)
        return -1;
    return 0;
}

s32 update_sessions(t_server *server) {
    t_session *session;
    s32 i;
    //u8 session_id_str[6];

    if (length_queue(&server->queue) >= MIN_PLAYERS) {
        if (create_new_session(server) != 0)
            return -1;
    }
    i = 0;
    while (i < vector_total(&server->sessions)) {
        session = vector_get(&server->sessions, i);
        update_session(session);
        /*if (session_finished(session)) {
            sprintf((char *) session_id_str, "%hu", session->identifier);
            if (remove_node_hashtable(&server->session_ids, (char *) session_id_str, false, NULL) != HASH_SUCCESS)
                return -1;
            free_session(session);
            vector_delete(&server->sessions, i);
        } else*/
        i++;
    }
    return 0;
}

s32 run_server(t_server *server) {
    s32 err;
    u64 start_time;

    err = 0;
    while (server->running) {
        start_time = get_current_time();
        if (server->status != 0) {
            printf("Failed to accept a client!\n");
            server->running = false;
            err = -1;
        }
        if (parse_packets(server) != 0)
            return -1;
        if (update_sessions(server) != 0)
            return -1;
        if (get_current_time() - start_time < ITERATION_TIME)
            delay(ITERATION_TIME - (get_current_time() - start_time));
    }
    return err;
}

static void free_clients(t_server *server) {
    s32 i;
    t_tsocket *client;

    for (i = 0; i < vector_total(&server->clients); i++) {
        client = vector_get(&server->clients, i);
        if (client)
            destroy_tsocket(client);
    }
}

void free_server(t_server *server) {
    server->running = false;
    free_accept_loop(server);
    free_clients(server);
    vector_free(&server->clients);
    vector_free(&server->sessions);
    free_queue(&server->queue, 0, NULL);
}