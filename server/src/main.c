#include "threaded_socket.h"
#include "server_loop.h"

#include <stdio.h>
#include <stdlib.h>

int main() {
    t_socket server_sock;
    t_server server;
    socket_status status;

    status = new_server(&server_sock, (s8 *)"0.0.0.0", 8181, MAX_USERS);
    if (status != SOCKET_OK) {
        printf("Failed to create server with error: %s\n", get_string_socket_status(status));
        return status;
    }
    if (init_server(&server, &server_sock) != 0) {
        printf("Failed to initialize the server!\n");
        return -1;
    }
    status = run_server(&server);
    close_socket(&server_sock);
    free_server(&server);
    return status;
}