//
// Created by kohlten on 4/13/19.
//

#ifndef SERVER_LOOP_H
#define SERVER_LOOP_H

#include <hashtable.h>
#include "vector_array.h"
#include "threaded_socket.h"

#define MAX_USERS 1000

typedef struct s_server t_server;

struct s_server {
    t_socket *server;
    vector clients;
    vector sessions;
    t_queue queue;
    pthread_t accept_loop;
    s32 status;
    u16 max_session_identifier;
    t_hashtable session_ids;
    bool running;
};

s32 init_server(t_server *server, t_socket *server_sock);
s32 run_server(t_server *server);
void free_server(t_server *server);


#endif //SERVER_LOOP_H
