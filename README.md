A recreation of Tron. Mostly made this as a networking exercise.

Requirements to compile:
    Meson
    Ninja
    Git
    Vim (Or your favorite text editor)
    Gcc
    
Required libraries to compile:
    CSFML
    SFML

To compile you will need several of my other projects, and to add their location to PKG_CONFIG_PATH before compiling.

Initial setup:
    (this project)
    git clone https://gitlab.com/kohlten/c_tron_new.git
    cd c_tron_new
    git clone https://gitlab.com/kohlten/c_vector.git
    git clone https://gitlab.com/kohlten/c_data_structures.git
    git clone https://gitlab.com/kohlten/threaded_socket.git
    
Change the pkg-config files:
    cd c_vector
    pwd
    (copy the output of pwd ^)
    vim vector.pc
    (then change the first line that says 'prefix=<filepath>' to 'prefix=<insert pwd output here>'. Then hit esc :wq)
    cd ..
    cd c_data_structures
    pwd
    (copy the output of pwd ^)
    vim data_structures.pc
    (then change the first line that says 'prefix=<filepath>' to 'prefix=<insert pwd output here>'. Then hit esc :wq)
    cd ..
    cd threaded_socket
    pwd
    (copy the output of pwd ^)
    vim threaded_socket.pc
    (then change the first line that says 'prefix=<filepath>' to 'prefix=<insert pwd output here>'. Then hit esc :wq)
    cd ..

Steps to compile:
    cd c_vector
    meson build
    cd build
    ninja
    cp libvector.a ..
    cd ../..
    cd c_data_structures
    meson build
    cd build
    ninja
    cp libdata_structures.a ..
    cd ../..
    cd threaded_socket
    meson build
    cd build
    ninja
    cp libthreaded_socket.a ..
    cd ../..
    cd c_tron_new/packet_parser
    meson build
    cd build
    ninja
    
    
    