//
// Created by kohlten on 3/16/19.
//

#include "game.h"

/* @TODO Add a script for the pkg-config files */

int main() {
    t_game game;

    if (init_game(&game) < 0)
        return -1;
    run_game(&game);
    clean_game(&game);
    return 0;
}
