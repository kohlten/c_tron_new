//
// Created by kohlten on 4/16/19.
//

#ifndef CLIENT_NETWORK_H
#define CLIENT_NETWORK_H

#include "packet_parser.h"
#include "threaded_socket.h"

void client_join_queue(t_tsocket *client);
void client_leave_queue(t_tsocket *client);
void client_send_start_ok(t_tsocket *client, u16 session_id);
void client_send_dir(t_tsocket *client, u16 session_id, u16 cycle_id, u16 dir);

#endif //CLIENT_NETWORK_H
