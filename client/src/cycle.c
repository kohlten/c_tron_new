//
// Created by kohlten on 3/21/19.
//

#include "cycle.h"

#include <strings.h>
#include <stdlib.h>

int init_cycle(t_cycle *cycle, u16 cycle_id) {
    bzero(cycle, sizeof(t_cycle));
    switch (cycle_id) {
        case 3:
            cycle->dir = CYCLE_DOWN;
            cycle->position = new_vector2f(0, 0);
            break;
        case 2:
            cycle->dir = CYCLE_LEFT;
            cycle->position = new_vector2f(0, MAP_HEIGHT - 1);
            break;
        case 1:
            cycle->dir = CYCLE_UP;
            cycle->position = new_vector2f(MAP_WIDTH - 1, MAP_HEIGHT - 1);
            break;
        case 0:
            cycle->dir = CYCLE_RIGHT;
            cycle->position = new_vector2f(MAP_WIDTH - 1, 0);
            break;
        default:
            break;
    }
    vector_init(&cycle->previous_positions);
    return 0;

}

/*
@TODO Perhaps later change the positions to be drawn at the same time as
@TODO currently drawing this many circle shapes results in a significant performance drop.
*/
int draw_cycle(t_cycle *cycle, sfRenderWindow *window) {
    sfCircleShape *circle;
    t_vector2f *pos;
    s32 i;

    circle = sfCircleShape_create();
    if (!circle)
        return -1;
    sfCircleShape_setFillColor(circle, sfGreen);
    sfCircleShape_setPosition(circle, vec_to_sfml_vec2f(cycle->position));
    sfCircleShape_setRadius(circle, 5);
    sfRenderWindow_drawCircleShape(window, circle, NULL);
    sfCircleShape_setRadius(circle, 2);
    for (i = 0; i < vector_total(&cycle->previous_positions); i++) {
        pos = vector_get(&cycle->previous_positions, i);
        if (pos) {
            sfCircleShape_setPosition(circle, (sfVector2f){pos->x, pos->y});
            sfRenderWindow_drawCircleShape(window, circle, NULL);
        }
    }
    sfCircleShape_destroy(circle);
    return 0;
}

s32 set_cycle_pos(t_cycle *cycle, t_vector2f pos) {
    t_vector2f *new;

    new = malloc(sizeof(t_vector2f));
    if (!new)
        return -1;
    *new = new_vector2f(cycle->position.x, cycle->position.y);
    vector_add(&cycle->previous_positions, new);
    cycle->position = pos;
    return 0;
}


/*int update_cycle(t_cycle *cycle, t_socket *client) {
    t_vector2f *new;
    t_vector2f new_pos;
    s8 packet[1024];
    u64 bytes;
    u64 i;

    receive_data_socket(client->socket_fd, 1024, packet, &bytes);
    if (bytes < 15)
        return -1;
    new_pos.x = atof((char *)packet + 3);
    for (i = 3; i < bytes && packet[i] != SEPERATOR; i++)
        ;
    i++;
    new_pos.y = atof((char *)packet + i);
    printf("Pos: %f %f\n", new_pos.x, new_pos.y);
    new = malloc(sizeof(t_vector2f));
    if (!new)
        return -1;
    *new = new_vector2f(cycle->position.x, cycle->position.y);
    vector_add(&cycle->previous_positions, new);
    cycle->position = new_pos;
    return 0;
}*/

void free_cycle(t_cycle *cycle) {
    vector_free(&cycle->previous_positions);
}



