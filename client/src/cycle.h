//
// Created by kohlten on 4/13/19.
//

#ifndef CLIENT_CYCLE_H
#define CLIENT_CYCLE_H

#include <SFML/Graphics.h>

#include "vector_array.h"
#include "number.h"

#define SFML_CONVERSION
#include "vector.h"

#define MAP_HEIGHT 400
#define MAP_WIDTH 400

typedef struct s_cycle t_cycle;
typedef enum e_cycle_dir t_cycle_dir;

enum e_cycle_dir
{
    CYCLE_UP = 1,
    CYCLE_DOWN = 2,
    CYCLE_LEFT = 3,
    CYCLE_RIGHT = 4,
    CYCLE_NONE = 0
};

struct s_cycle {
    t_vector2f position;
    t_cycle_dir dir;
    vector previous_positions;
};

int init_cycle(t_cycle *cycle, u16 cycle_id);
int draw_cycle(t_cycle *cycle, sfRenderWindow *window);
s32 set_cycle_pos(t_cycle *cycle, t_vector2f pos);
//int update_cycle(t_cycle *cycle, t_socket *client);
void free_cycle(t_cycle *cycle);

#endif //CLIENT_CYCLE_H
