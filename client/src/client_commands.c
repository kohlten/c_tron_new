//
// Created by kohlten on 4/17/19.
//

#include "client_commands.h"
#include "threaded_socket.h"
#include "packet_parser.h"
#include "cycle.h"

#include <stdlib.h>
#include <stdio.h>

void network_set_cycle_identity(t_game *game, t_packet *packet) {
    game->cycle_id = packet->cycle_identifier;
}

void network_set_session_identity(t_game *game, t_packet *packet) {
    game->session_id = packet->session_identifier;
}

s32 network_set_cycle_pos(t_game *game, t_packet *packet) {
    if (game->running) {
        printf("Setting cycle %d to pos %f %f\n", packet->cycle_identifier, packet->pos.x, packet->pos.y);
        return set_cycle_pos(&game->cycles[packet->cycle_identifier], packet->pos);
    }
    return 0;
}

void set_game_state(t_game *game) {
    game->playing = true;
}

s32 cycle_command(t_game *game, t_packet *packet) {
    switch (packet->cycle_action) {
        case CYCLE_POS:
            return network_set_cycle_pos(game, packet);
    }
    return 0;
}

s32 map_command(t_game *game, t_packet *packet) {
    switch (packet->action) {
        case CYCLE_ACTION:
            return cycle_command(game, packet);
        case SESSION_IDENTIFIER:
            network_set_session_identity(game, packet);
            return 0;
        case CYCLE_IDENTIFIER:
            network_set_cycle_identity(game, packet);
            return 0;
        case SESSION_START:
            set_game_state(game);
            return 0;
    }
    return -1;
}

s32 recieve_loop(t_game *game) {
    t_packet *packet;
    u8 *data;
    u8 *data_tmp;
    u64 bytes;
    u64 end;
    t_tsocket_status status;

    if (data_available_tsocket(&game->client) > 0) {
        data_tmp = peek_data_tsocket(&game->client, &bytes, &status);
        if (status != TSOCKET_OK)
            return -1;
        if (is_valid_packet(data_tmp, bytes, &end) != -1) {
            do {
                packet = commander_parser_parse(data_tmp, bytes);
                if (!packet)
                    return -1;
                if (map_command(game, packet) != 0)
                    return -1;
                bytes -= end + 1;
                data_tmp += end + 1;
            } while (is_valid_packet(data_tmp, bytes, &end) != -1);
            data = get_data_tsocket(&game->client, &bytes, &status);
            if (status != TSOCKET_OK)
                return -1;
            free(data);
        }
    }
    return 0;
}