//
// Created by kohlten on 3/21/19.
//

#include "window.h"

#include <strings.h>

static void set_window_positon(sfRenderWindow *window, sfVector2i offset, sfVideoMode monitor_size) {
    sfVector2u size;
    sfVector2i new_pos;

    size = sfRenderWindow_getSize(window);
    new_pos.x = (monitor_size.width / 2 - size.x / 2) + offset.x;
    new_pos.y = (monitor_size.height / 2 - size.y / 2) + offset.y;
    sfRenderWindow_setPosition(window, new_pos);

}

sfRenderWindow *new_window(u32 xsize, u32 ysize, u8 *title, u8 monitor) {
    sfRenderWindow *window;
    sfContextSettings settings;
    u64 monitors;
    sfVector2i offset;
    u32 i;
    const sfVideoMode *modes;

    bzero(&settings, sizeof(sfContextSettings));
    window = sfRenderWindow_create((sfVideoMode){xsize, ysize, 32}, (char *)title, sfResize | sfClose, NULL);
    if (!window)
        return NULL;
    monitors = 0;
    modes = sfVideoMode_getFullscreenModes((size_t *)&monitors);
    if (!modes)
        set_window_positon(window, (sfVector2i){0, 0}, sfVideoMode_getDesktopMode());
    else {
        if (monitor > monitors)
            goto clean;
        bzero(&offset, sizeof(sfVector2i));
        for (i = 0; i < monitor; i++) {
            offset.x += modes[i].width;
            offset.y += modes[i].height;
        }
        set_window_positon(window, offset, modes[monitor]);
    }
    return window;
    clean:
    sfRenderWindow_close(window);
    return NULL;
}