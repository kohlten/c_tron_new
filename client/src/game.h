//
// Created by kohlten on 3/21/19.
//

#ifndef CLIENT_GAME_H
#define CLIENT_GAME_H

#include "window.h"
#include "cycle.h"
#include "threaded_socket.h"

#include <stdbool.h>

#define MAX_PLAYERS 4

typedef struct s_game t_game;

struct s_game {
    sfRenderWindow *window;
    bool running;
    bool playing;
    bool send_ok;
    bool dir_changed;
    t_cycle cycles[MAX_PLAYERS];
    t_tsocket client;
    u16 session_id;
    u16 cycle_id;
};

int init_game(t_game *game);
void run_game(t_game *game);
void clean_game(t_game *game);

#endif //CLIENT_GAME_H
