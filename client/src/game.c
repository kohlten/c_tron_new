//
// Created by kohlten on 3/21/19.
//

#include "game.h"

#include "threaded_socket.h"
#include "client_network.h"
#include "client_commands.h"

#include <stdio.h>
#include <strings.h>

// @TODO Add game config for settings
int init_game(t_game *game) {
    bzero(game, sizeof(t_game));
    game->window = new_window(800, 800, (u8 *)"TRON CLIENT", 0);
    if (!game->window) {
        printf("Failed to make the render window!\n");
        return -1;
    }
    if (init_tsocket_client(&game->client, (const s8 *)"0.0.0.0", 8181) != TSOCKET_OK)
        return -1;
    sfRenderWindow_setFramerateLimit(game->window, 60);
    game->running = true;
    game->playing = false;
    game->send_ok = false;
    game->dir_changed = true;
    return 0;
}

static void game_events(t_game *game) {
    sfEvent e;

    while (sfRenderWindow_pollEvent(game->window, &e)) {
        if (e.type == sfEvtClosed)
            game->running = false;
        if (e.type == sfEvtKeyPressed) {
            if (e.key.code == sfKeyEscape)
                game->running = false;
            if (e.key.code == sfKeyJ)
                client_join_queue(&game->client);
            if (e.key.code == sfKeyL)
                client_leave_queue(&game->client);
        }
    }
}

static void game_update(t_game *game) {
    s32 i;

    if (recieve_loop(game) != 0) {
        printf("Failed to parse packets!\n");
    }
    if (game->session_id > 0 && !game->playing && !game->send_ok) {
        for (i = 0; i < MAX_PLAYERS; i++)
            init_cycle(&game->cycles[i], i);
        client_send_start_ok(&game->client, game->session_id);
        game->send_ok = true;
    }
    if (game->playing && game->send_ok) {
        client_send_dir(&game->client, game->session_id, game->cycle_id, game->cycles[game->cycle_id].dir);
        game->dir_changed = false;
    }
}

static void game_draw(t_game *game) {
    u32 i;

    sfRenderWindow_clear(game->window, sfRed);
    if (game->playing) {
        for (i = 0; i < MAX_PLAYERS; i++)
            draw_cycle(&game->cycles[i], game->window);
    }
    sfRenderWindow_display(game->window);
}

void run_game(t_game *game) {
    while (game->running) {
        game_events(game);
        game_update(game);
        game_draw(game);
    }
}

void clean_game(t_game *game) {
    u32 i;

    for (i = 0; i < MAX_PLAYERS; i++) {
        free_cycle(&game->cycles[i]);
    }
    sfRenderWindow_close(game->window);
}