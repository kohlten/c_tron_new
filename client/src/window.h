//
// Created by kohlten on 4/13/19.
//

#ifndef CLIENT_WINDOW_H
#define CLIENT_WINDOW_H

#include <SFML/Graphics.h>

#include "number.h"

sfRenderWindow *new_window(u32 xsize, u32 ysize, u8 *title, u8 monitor);

#endif //CLIENT_WINDOW_H
