//
// Created by kohlten on 4/16/19.
//

#include "client_network.h"

#include <stdio.h>

void send_basic_packet(t_tsocket *client, u8 action) {
    u8 packet[4];

    sprintf((char *) packet, "%c%c%c", START_MESSAGE, action, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}

void client_join_queue(t_tsocket *client) {
    send_basic_packet(client, ADD_TO_QUEUE);
    printf("Sent packet to be added to queue!\n");
}

void client_leave_queue(t_tsocket *client) {
    send_basic_packet(client, REMOVE_FROM_QUEUE);
    printf("Sent packet to be removed from the queue!\n");
}

void client_send_start_ok(t_tsocket *client, u16 session_id) {
    u8 packet[10];

    sprintf((char *) packet, "%c%c%c%hu%c", START_MESSAGE, CLIENT_OK_START, SEPERATOR, session_id, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
    printf("Send client ok!\n");
}

void client_send_dir(t_tsocket *client, u16 session_id, u16 cycle_id, u16 dir) {
    u8 packet[24];

    sprintf((char *) packet, "%c%c%c%c%c%hu%c%hu%c%hu%c", START_MESSAGE, CYCLE_ACTION, SEPERATOR, CYCLE_DIR, SEPERATOR,
            session_id, SEPERATOR, cycle_id, SEPERATOR, dir, END_MESSAGE);
    send_terminated_data_tsocket(client, packet);
}